//  Task ventilator
//  Binds PUSH socket to tcp://localhost:5557
//  Sends batch of tasks to workers via that socket
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <nng/nng.h>
#include <nng/protocol/pipeline0/push.h>
#include "pipeline.h"

#define within(num) (int) ((float) num * random () / (RAND_MAX + 1.0))

/**
 * Signal the sink we are ready.
 */
void
tell_sink(void)
{
    nng_socket sock;
    int rv;
    char msg[2];

    if ((rv = nng_push0_open(&sock)) != 0) {
        fatal("nng_push0_open", rv);
    }
    if ((rv = nng_dial(sock, tcp_sink, NULL, 0)) != 0) {
        fatal("nng_dial", rv);
    }
    //  The first message is "0" and signals start of batch
    sprintf(msg, "0");
    if ((rv = nng_send(sock, msg, strlen(msg)+1, 0)) != 0) {
        fatal("nng_send", rv);
    }

    // You may need a sleep() here in case the socket gets destroyed too fast
    // and you lose the message.
}

int
main(void)
{
    //  Socket to send messages on
    nng_socket sock;
    int rv;

    if ((rv = nng_push0_open(&sock)) != 0) {
        fatal("nng_push0_open", rv);
    }
    if ((rv = nng_listen(sock, tcp_ventilator, NULL, 0)) != 0) {
        fatal("nng_listen", rv);
    }

    printf("Press Enter when the workers are ready: ");
    getchar();
    tell_sink();

    //  Initialize random number generator
    srandom((unsigned) time(NULL));

    //  Send 100 tasks
    printf("Sending tasks to workers…\n");
    int total_msec = 0;     //  Total expected cost in msecs
    for (int task_nbr = 0; task_nbr < 100; task_nbr++) {
        int workload;
        char msg[4];            // Careful with the size of `within` + '\0'

        //  Random workload from 1 to 100msecs
        workload = within(100) + 1;
        total_msec += workload;
        sprintf(msg, "%d", workload);

        if ((rv = nng_send(sock, msg, strlen(msg) + 1, 0)) != 0) {
            fatal("nng_send", rv);
        }
    }
    printf("Total expected cost: %d msec\n", total_msec);
    // Again, the 0MQ example has a sleep here.

    return 0;
}
