# Fan-in/Fan-out Pipeline in NNG

Code to convert the ØMQ
[example](http://zguide.zeromq.org/page:all#Divide-and-Conquer " ") to NNG.
The trick is to use `listen` on the push socket and `dial` on the workers.

Thanks to `nico` on the NNG's Discord for the pointers.

## Compilation

You have to have NNG installed, then it's just:

```bash
$ gcc -o ventilator -pthread -lnng ventilator.c pipeline.h
$ gcc -o worker -pthread -lnng worker.c pipeline.h
$ gcc -o sink -pthread -lnng sink.c pipeline.h
```
