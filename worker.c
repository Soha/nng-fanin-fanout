//  Task worker
//  Connects PULL socket to tcp://localhost:5557
//  Collects workloads from ventilator via that socket
//  Connects PUSH socket to tcp://localhost:5558
//  Sends results to sink via that socket
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <nng/nng.h>
#include <nng/protocol/pipeline0/pull.h>
#include <nng/protocol/pipeline0/push.h>
#include <nng/supplemental/util/platform.h>

#include "pipeline.h"

int
main(void)
{
    int rv;
    nng_socket receiver, sender;

    if ((rv = nng_pull0_open(&receiver)) != 0) {
        fatal("nng_pull0_open", rv);
    }
    if ((rv = nng_dial(receiver, tcp_ventilator, NULL, 0)) != 0) {
        fatal("nng_dial: ventilator", rv);
    }
    printf("worker: LISTEN on \"%s\"\n", tcp_ventilator);

    //  Socket to send messages to
    if ((rv = nng_push0_open(&sender)) != 0) {
        fatal("nng_push0_open", rv);
    }

    if ((rv = nng_dial(sender, tcp_sink, NULL, 0)) != 0) {
        fatal("nng_dial: sink", rv);
    }
    printf("worker: SEND to \"%s\"\n", tcp_sink);

    //  Process tasks forever
    while (1) {
        char *buf = NULL;
        size_t sz;
        int workload;           //  Workload in msecs

        if ((rv = nng_recv(receiver, &buf, &sz, NNG_FLAG_ALLOC)) != 0) {
            fatal("nng_recv", rv);
        }

        workload = atoi(buf);

        //  Do the work
        nng_msleep(workload);

        //  Send results to sink
        if ((rv = nng_send(sender, buf, strlen(buf) + 1, 0)) != 0) {
            fatal("nng_send", rv);
        }

        printf(".");     //  Show progress
        fflush(stdout);
        nng_free(buf, sz);
    }
    return 0;
}
