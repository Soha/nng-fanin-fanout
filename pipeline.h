// Convenience header
#ifndef __PIPELINE_H_
#define __PIPELINE_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <nng/nng.h>

char* tcp_ventilator = "tcp://localhost:5777";
char* tcp_sink = "tcp://localhost:5778";

static void
fatal(const char *func, int rv) {
    fprintf(stderr, "%s: %s\n", func, nng_strerror(rv));
    exit(1);
}


#endif // __PIPELINE_H_
