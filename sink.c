//  Task sink
//  Binds PULL socket to tcp://localhost:5558
//  Collects results from workers via that socket
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>

#include <nng/nng.h>
#include <nng/protocol/pipeline0/pull.h>
#include <nng/supplemental/util/platform.h>

#include "pipeline.h"

int
main(void)
{
    //  Prepare our socket
    nng_socket sock;
    int rv;

    if ((rv = nng_pull0_open(&sock)) != 0) {
        fatal("nng_pull0_open", rv);
    }
    if ((rv = nng_listen(sock, tcp_sink, NULL, 0)) != 0) {
        fatal("nng_listen", rv);
    }
    printf("sink: LISTEN on \"%s\"\n", tcp_sink);

    //  Wait for start of batch
    char *buf = NULL;
    size_t sz;
    if ((rv = nng_recv(sock, &buf, &sz, NNG_FLAG_ALLOC)) != 0) {
        fatal("nng_recv", rv);
    }

    // Could do some check to make sure we received "0"
    printf("sink: RECEIVED \"%s\"\n", buf);
    nng_free(buf, sz);

    //  Start our clock now
    uint64_t start_time = nng_clock();
    //  Process 100 confirmations
    int total_msec = 0;     //  Total calculated cost in msecs
    for (int task_nbr = 0; task_nbr < 100; task_nbr++) {
        if ((rv = nng_recv(sock, &buf, &sz, NNG_FLAG_ALLOC)) != 0) {
            fatal("nng_recv", rv);
        }
        total_msec += atoi(buf);
        nng_free(buf, sz);

        if ((task_nbr / 10) * 10 == task_nbr) {
            printf(":");
        } else {
            printf(".");
        }
        fflush(stdout);
    }
    //  Calculate and report duration of batch
    printf("\n");
    printf("Total elapsed time: %d / %d msec\n",
           (int) (nng_clock() - start_time), total_msec);

    return 0;
}
